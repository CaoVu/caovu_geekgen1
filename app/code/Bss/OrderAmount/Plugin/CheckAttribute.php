<?php

namespace Bss\OrderAmount\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\RequestInterface;

class CheckAttribute
{

    public function __construct(
        ResultFactory $Redirect,
        ManagerInterface $messageManager,
        RequestInterface $request
    ) {
        $this->resultFactory = $Redirect;
        $this->_messageManager = $messageManager;
        $this->getRequest = $request;
    }

    public function aroundexecute(
        \Magento\Customer\Controller\Adminhtml\Index\Save $subject,
        $proceed,
        $data = "null",
        $requestInfo = false
    ) {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $data = $subject->getRequest()->getPostValue();

        if (isset($data['customer']) || $data['customer'] != '') {
            //code to validate start and end date
            if (is_numeric($data['customer']['minimum_order_amount'])==false) {
                $this->_messageManager->addError(__('Minimum Order Amount must be number!!'));
                return $resultRedirect->setRefererOrBaseUrl();
            } elseif (floatval($data['customer']['minimum_order_amount'])<0) {
                $this->_messageManager->addError(__('Minimum Order Amount not allowed negative number !! '));
                return $resultRedirect->setRefererOrBaseUrl();
            }
        }
        return $proceed();
    }
}
