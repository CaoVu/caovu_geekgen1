<?php

namespace Bss\Schema\Observer;

use Bss\Schema\Model\ResourceModel\DataExample;

class Observer implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Bss\Schema\Model\ResourceModel\InternData\Collection
     */
    protected $collectionFactory;
    /**
     * @var InternDataFactory
     */

    public function __construct(
        \Bss\Schema\Model\ResourceModel\InternData                   $internData,
        \Bss\Schema\Model\ResourceModel\InternData\CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->internData = $internData;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $customer = $observer->getEvent()->getCustomer();
        $collections = $this->collectionFactory->create();
        $collections->addFieldToFilter('email',['eq' => $customer->getEmail()]);

        foreach ($collections as $item) {
            $item->setName($customer->getFirstName() . ' ' . $customer->getLastName());
        }
        $collections->save();

    }
}
