<?php
namespace Bss\Schema\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class ObserverSaveCustomVat implements ObserverInterface
{
    /**
     * save value custom_vat field to table sales_order
     *
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer): ObserverSaveCustomVat
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        $order->setCustomVat($quote->getShippingAddress()->getCustomVat());
        $order->save();
        return $this;
    }
}
