<?php
namespace Bss\Schema\Model\ResourceModel\InternData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName= 'id';
    /**
     *
     */
    public function _construct()
    {
        $this->_init("Bss\Schema\Model\InternData", "Bss\Schema\Model\ResourceModel\InternData");
    }
}
