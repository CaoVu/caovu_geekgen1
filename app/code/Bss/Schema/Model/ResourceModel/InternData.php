<?php
namespace Bss\Schema\Model\ResourceModel;

class InternData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init("intern_data", "id");
    }

}
