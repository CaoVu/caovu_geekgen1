<?php

namespace Bss\Schema\Plugin;

class OrdersGrid
{
    public function afterGetReport($subject, $collection, $requestName)
    {
        if ($requestName !== 'sales_order_grid_data_source') {
            return $collection;
        }

        if ($collection->getMainTable() === $collection->getResource()->getTable('sales_order_grid')) {
            $orderAddressTable  = $collection->getResource()->getTable('quote_address');

            $collection->getSelect()->joinLeft(
                ['oat' => $orderAddressTable],
                'oat.quote_id = main_table.entity_id AND oat.address_type = \'shipping\'',
                ['custom_vat']
            );
        }

        return $collection;
    }
}
