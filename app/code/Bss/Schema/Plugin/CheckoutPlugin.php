<?php

/**
 *
 */

namespace Bss\Schema\Plugin;
/**
 *
 */
class CheckoutPlugin
{

    public function __construct(
        \Magento\Framework\Message\ManagerInterface                  $messageManager,
        \Bss\Schema\Model\ResourceModel\InternData\CollectionFactory $collectionFactory
    )
    {
        $this->messageManager = $messageManager;
        $this->collectionFactory = $collectionFactory;
    }




    public function afterExecute(
        \Magento\Checkout\Controller\Cart\Index $subject,
                                                       $result
    )
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $om->get('Magento\Customer\Model\Session');
        $customerEmail = $customerSession->getCustomer()->getEmail();
        if (isset($customerEmail)) {
            $collections = $this->collectionFactory->create();
            $data = $collections->addFieldToFilter('email', ['eq' => $customerEmail]);
            if (count($data) > 0) {
                $this->messageManager->addSuccessMessage(__('You got 50% off all orders'));
            }
        }
        return $result;
    }
}
