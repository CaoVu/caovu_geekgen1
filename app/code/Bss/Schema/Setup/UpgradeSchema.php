<?php
namespace Bss\Schema\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;

        $installer->startSetup();
        $quote = 'quote';
        $orderGrid='sales_order';
        $orderTable = 'sales_order_grid';

        if(version_compare($context->getVersion(), '1.0.3', '<')) {
            $installer->getConnection()
                ->addColumn(
                    $installer->getTable($quote),
                    'custom_vat',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Custom Vat'
                    ]
                );
                //Order grid
            $installer->getConnection()
                ->addColumn(
                    $installer->getTable($orderGrid),
                    'custom_vat',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Custom Vat'
                    ]
                );
            //Order table
            $installer->getConnection()
                ->addColumn(
                    $installer->getTable($orderTable),
                    'custom_vat',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Custom Vat'
                    ]
                );
        }

        if(version_compare($context->getVersion(), '1.0.3', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable( 'intern_data' ),
                'status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'default' => 1,
                    'comment' => 'Status'
                ]
            );
        }
        $installer->endSetup();
    }
}
