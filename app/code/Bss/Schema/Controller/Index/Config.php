<?php

namespace Bss\Schema\Controller\Index;

use Bss\Schema\Helper\Data;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Forward;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\ResultInterface;

class Config extends \Magento\Framework\App\Action\Action
{

    protected Data $helperData;
    protected ForwardFactory $_resultForwardFactory;

    /**
     * @param Context $context
     * @param Data $helperData
     * @param ForwardFactory $_resultForwardFactory
     */
    public function __construct(
        Context        $context,
        Data           $helperData,
        ForwardFactory $_resultForwardFactory
    ) {
        $this->_resultForwardFactory = $_resultForwardFactory;
        $this->helperData = $helperData;
        parent::__construct($context);
    }

    /**
     *
     * @return ResponseInterface|Forward|ResultInterface|void
     *
     */

    public function execute()
    {

        // TODO: Implement execute() method.

        if ($this->helperData->getGeneralConfig('enable')) {
            $resultForward = $this->_resultForwardFactory->create();
            $resultForward->setController('index')
                ->setModule('schema')
                ->forward('intern');
            return $resultForward;
        } else {
            $resultForward = $this->_resultForwardFactory->create();
            $resultForward->setController('index')
                ->setModule('cms')
                ->forward('index');
            $this->messageManager->addError(__('You do not have enough permissions to access this page, please contact the administrator!!!'));
            return $resultForward;
        }
    }
}
