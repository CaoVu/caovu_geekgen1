<?php
namespace Bss\Schema\Controller\Index;

use Bss\Schema\Model\InternDataFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;

class Create extends \Magento\Framework\App\Action\Action
{
    protected InternDataFactory $internData;
    protected ResultFactory $resultRedirect;
    protected RequestInterface $request;

    /**
     *
     * @param RequestInterface $request
     * @param Context $context
     *
     * @param InternDataFactory $internData
     *
     * @param ResultFactory $result
     */
    public function __construct(
        RequestInterface $request,
        \Magento\Framework\App\Action\Context $context,
        \Bss\Schema\Model\InternDataFactory  $internData,
        \Magento\Framework\Controller\ResultFactory $result
    ) {
        parent::__construct($context);
        $this->internData = $internData;
        $this->resultRedirect = $result;
        $this->request = $request;
    }

    /**
     * Add data
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        $params = $this->request->getParams();
//Check if params had 2 or more consecutive space and validate input
        $data=(implode("", array_values($params)));
        if (preg_match("/^[a-zA-Z0-9 @.-]+$/", $data)==0 || strpos($data, '  ') !== false) {
            $this->messageManager->addError(__('There was an error while saving Internship data, please try again!!'));
        } else {
            $model = $this->internData->create();
            $model->addData([
                "name" => $params['name'],
                "dob" => $params['dob'],
                "address" => $params['address'],
                "gender" => $params['gender'],
                "email" => $params['email'],
                "sort_order" => 1
            ]);
            try {
                $saveData = $model->save();
                if ($saveData) {
                    $this->messageManager->addSuccess(__('Insert Record Successfully !'));
                }
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Error 69 !'));
            }
        }
        return $resultRedirect;
    }
}
