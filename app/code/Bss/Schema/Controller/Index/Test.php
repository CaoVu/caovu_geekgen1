<?php

namespace Bss\Schema\Controller\Index;

class Test extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $customerSession;
    public function __construct(
        \Magento\Framework\App\Action\Context             $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customerRepositoryInterface = $customerRepositoryInterface;

    }

    public function execute()
    {
        var_dump($this->customerSession->getCustomer()->getData());
        $customerId = $this->customerSession->getCustomer()->getId();
        var_dump($this->customerRepositoryInterface->getById($customerId)->getCustomAttribute('minimum_order_amount')->getValue());
        die;

    }

}
