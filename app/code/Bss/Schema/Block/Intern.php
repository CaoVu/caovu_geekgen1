<?php
namespace Bss\Schema\Block;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template\Context;

class Intern extends \Magento\Framework\View\Element\Template
{
    protected RequestInterface $request;
    Const Get_Action="https://m243.test/schema/index/create";
    /**
     *
     * @param Context $context
     * @param RequestInterface $request
     */
    public function __construct(
        Context $context,
        RequestInterface $request
    )
    {
        parent::__construct($context);
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return self::Get_Action;
    }


}
