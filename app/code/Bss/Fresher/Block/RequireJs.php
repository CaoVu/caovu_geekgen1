<?php
namespace Bss\Fresher\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template\Context;

class RequireJs extends \Magento\Framework\View\Element\Template
{
    protected RequestInterface $request;

    /**
     * @param Context $context
     * @param RequestInterface $request
     */
    public function __construct(
        Context $context,
        RequestInterface $request
    ) {
        parent::__construct($context);
        $this->request = $request;
    }

}
