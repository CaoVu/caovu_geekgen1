define([
        'jquery',
        "Magento_Ui/js/modal/modal"
    ],
    function ($, modal) {
        "use strict";

        $.widget('mage.widgetjs', {
            options: {
                Aform: "#form",
            },
            _create: function () {
                this.submits();
            },

            //function submit form
            submits: function () {
                var options = {
                    type: 'popup',
                    responsive: true,
                    title: 'Your information',
                    buttons: [{
                        text: $.mage.__('Ok'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };
                modal(options, $('#modal'));
                $(this.options.Aform).on('submit', function (event) {
                    event.preventDefault();
                    // eslint-disable-next-line no-alert
                    const product = {
                        name: event.target.name.value,
                        telephone: event.target.telephone.value,
                        date: event.target.date.value,
                        message: event.target.message.value
                    };
                    if (product.name === '' || product.telephone === '' || product.date === '' || product.message === '') {
                        alert("Data can not be null");
                    } else {
                        // format modal and input data
                        var load_data = `
                   <table >
                   <tr>
                       <td>Name:</td>
                       <td>${product.name}</td>
                   </tr>
                     <tr>
                       <td>Telephone:</td>
                        <td>${product.telephone}</td>
                    </tr>
                      <tr>
                        <td>Dob:</td>
                        <td>${product.date}</td>
                   </tr>
                      <tr>
                       <td>Message:</td>
                        <td>${product.message}</td>
                    </tr>
</table>
                    `;
                        document.getElementById("modal").innerHTML = load_data;
                        $('#modal').modal('openModal');
                    }
                });
            }
        });
        return $.mage.widgetjs;
    });
