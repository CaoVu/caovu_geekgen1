define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Bss_Fresher/knockout-test'
            },
            //function create and remove data
            initialize: function () {
                var child=this;
                this.customer= ko.observableArray([]);
                this.customerDataId= ko.observable('');
                this.customerDataName = ko.observable('');
                this.customerDataClass = ko.observable('');
                child.removeInfor=function (customer){
                    child.customer.remove(customer);
                };
                this._super();
            },

            //function to add new data
            addNewCustomer: function () {

                this.customer.push({
                    id:this.customerDataId(),
                    name:this.customerDataName(),
                    description:this.customerDataClass()
                });
                this.customerDataId('');
                this.customerDataName('');
                this.customerDataClass('');
            },

            //function remove all data
            removeList:function (){
                this.customer.removeAll();
            },


        });
    }
);
