<?php
namespace HelloWorld\Routing\Block;
class KoJs extends \Magento\Framework\View\Element\Template
{
    protected $request;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        parent::__construct($context);
        $this->request = $request;
    }

    public function sayHello()
    {
       echo "Knock Out Js";
    }

}
