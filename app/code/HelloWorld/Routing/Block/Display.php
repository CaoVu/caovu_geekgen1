<?php
namespace HelloWorld\Routing\Block;
class Display extends \Magento\Framework\View\Element\Template
{
    protected $request;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        parent::__construct($context);
        $this->request = $request;
    }

    public function sayHello()
    {
        return $this->request->getParams();
    }

}
