<?php
namespace HelloWorld\Routing\Block;
//use Magento\Framework\UrlFactory;
class Config extends \Magento\Framework\View\Element\Template
{
    protected $request;
//    protected $messageManager;
//    protected $urlModel;
    private  $helperData;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \HelloWorld\Routing\Helper\Data $helperData,
//        UrlFactory $urlFactory,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->helperData = $helperData;
//        $this->messageManager = $context->getMessageManager();
//        $this->urlModel = $urlFactory->create();
    }

    public function sayHello()
    {
//        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        if($this->helperData->getGeneralConfig('enable')==1)
        {
            $pageTitle=$this->helperData->getGeneralConfig('page_title');
            $pageDes=$this->helperData->getGeneralConfig('page_description');
            return compact('pageTitle','pageDes');
        }else{
//            $this->messageManager->addErrorMessage(__('Bad request.'));
//            $url = $this->urlModel->getUrl('*/*/index', ['_secure' => true]);
//            return $resultRedirect->setUrl($this->_redirect->error($url));
        }
    }

}
