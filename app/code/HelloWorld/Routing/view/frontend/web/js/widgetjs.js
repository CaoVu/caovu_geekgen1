define([
    'jquery',
    // 'jquery/ui',
    'Magento_Ui/js/modal/prompt'
// eslint-disable-next-line strict
],
    function ($,prompt) {
    "use strict";

    $.widget('mage.widgetjs', {
        options: {
            arr: [
                {
                    "name":"Cao Vu",
                    "price":50,
                    "description":"???",
                    "link":"Ka"
                }
            ],
            Alist: "#list",
            Aform: "#form",

        },

        _create: function () {
            this.loads();
                this.submits();
                this.deletes();
        },
        loads: function () {
            var load_data = this.options.arr.map(function (item, index) {
                return `<tr id="MyTr" >
                <td class="ids">${index + 1}</td>
                <td> ${item.name}</td>
                <td>${item.price}</td>
                <td>${item.description}</td>
                <td ><img src="${item.link}" alt="" style="height: 100px;" ></td>
                <td ><button id="DeleteButton"
                 class="btn" style="background-color: #b94a48">Delete</button></td>
        </tr>`
            }).join('');
            $(this.options.Alist).html(load_data);
        },

        submits: function () {
            var arr = this.options.arr;
            var loads = this;
            $(this.options.Aform).on('submit', function (event) {
                event.preventDefault();
                // eslint-disable-next-line no-alert
                const product = {
                    name: event.target.name.value,
                    price: event.target.price.value,
                    description: event.target.description.value,
                    link: event.target.link.value
                };
                if (product.name === '' || product.price === '' || product.description === '' || product.link === '') {
                    alert("Data can not be null");
                } else {
                    arr.push(product);
                    loads.loads();
                }
            });
        },

        prompts: function (arr,num){
            var loads = this;
            console.log("prompt1");
            prompt({
                title: $.mage.__('Are You sure to delete this data?'),
                promptContentTmpl:"",
                actions: {
                    always: function() {
                      loads.loads();
                    },
                    confirm: function () {
                        arr.splice(num-1,1);
                    },
                    cancel: function () {
                    }
                }
            });
        },

        deletes: function () {
            var arr = this.options.arr;
            var loads = this;
            console.log("delete");
            $("#list").on("click", "#DeleteButton", function () {
                var val = $(this).closest('tr').find(".ids").text();
                var num = parseInt(val);
                loads.prompts(arr,num);
            });
        }
    });
    return $.mage.widgetjs;
});
