define([
    "jquery",
    'jquery/ui',
], function ($) {
    "use strict";
    var arr = [
        {
            "name":"Cao Vu",
            "price":50,
            "description":"???",
            "link":"Ka"
        }
    ];


    function load() {
        var load_data = arr.map(function (item, index) {
            return `<tr id="MyTr" >
                <td class="ids">${index + 1}</td>
                <td> ${item.name}</td>
                <td>${item.price}</td>
                <td>${item.description}</td>
                <td ><img src="${item.link}" alt="" style="height: 100px;" ></td>
                <td ><button id="DeleteButton"
                 class="btn" style="background-color: #b94a48">Delete</button></td>
        </tr>`
        }).join('');
        document.getElementById("list").innerHTML = load_data;
    };
    load();


    $("#form").on('submit', function (event) {
        event.preventDefault();
        // eslint-disable-next-line no-alert

        const product = {
            name: event.target.name.value,
            price: event.target.price.value,
            description: event.target.description.value,
            link: event.target.link.value
        };
        if(product.name===''||product.price===''||product.description===''||product.link==='')
        {
            alert("Data can not be null")
        }else
            arr.push(product);
        load();
    });


    $("#list").on("click", "#DeleteButton", function() {
        var val = $(this).closest('tr').find(".ids").text();
        var num=parseInt(val);
        // alert(num);
        arr.splice(num-1, 1)
        // console.log(arr);
        load();
    });

});
