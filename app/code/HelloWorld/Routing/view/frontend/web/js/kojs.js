define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'HelloWorld_Routing/knockout-test'
            },
            initialize: function () {
                this.customerName = ko.observableArray([]);
                this.customerDataName = ko.observable('');
                this.customerPrice= ko.observableArray([]);
                this.customerDataPrice= ko.observable('');
                this.customerDescription=ko.observableArray([]);
                this.customerDataDes = ko.observable('');
                this._super();
            },

            addNewCustomer: function () {
                this.customerName.push({name:this.customerDataName()});
                this.customerDataName('');
                this.customerPrice.push({price:this.customerDataPrice()});
                this.customerDataPrice('');
                this.customerDescription.push({description:this.customerDataDes()});
                this.customerDataDes('');
            }
        });
    }
);
