<?php
namespace HelloWorld\Routing\Controller\Index;

class GetJson extends \Magento\Framework\App\Action\Action
{
    protected $request;
    protected $jsonResultFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    )
    {
        $this->request = $request;
        $this->jsonResultFactory = $jsonResultFactory;
        parent::__construct($context);

    }

    public function execute()
    {
        $params = $this->request->getParams();
        $result = $this->jsonResultFactory->create();
        $result->setData($params);
        return $result;
    }
}
