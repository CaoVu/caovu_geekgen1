<?php
namespace HelloWorld\Routing\Controller\Index;

class Custom extends \Magento\Framework\App\Action\Action
{
    protected $request;
    protected $jsonResultFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    )
    {
        $this->request = $request;
        $this->jsonResultFactory = $jsonResultFactory;
        parent::__construct($context);

    }

    public function execute()
    {
        echo"Intership-Course";
    }
}
