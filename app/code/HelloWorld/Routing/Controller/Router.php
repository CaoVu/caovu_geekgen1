<?php
declare(strict_types=1);

namespace HelloWorld\Routing\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $params = $this->request->getParams();
        $a= implode(array_keys($params));
        $identifier = trim($request->getPathInfo(), '/');

        if (strpos($identifier, 'intership-course') !== false) {
            $request->setModuleName('routing');
            $request->setControllerName('index');
            $request->setActionName('custom');
            $request->setParams([
                "$a"=>'1'
            ]);
            return $this->actionFactory->create(Forward::class, ['request' => $request]);
        }
        return null;
    }

}
